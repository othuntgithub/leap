package com.legacy.leap;

import java.util.logging.Logger;

import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;

@Mod(LeapMod.MODID)
public class LeapMod
{
	public static final String NAME = "Leap";
	public static final String MODID = "leap";
	public static Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public LeapMod()
	{
		MinecraftForge.EVENT_BUS.register(new LeapEntityEvents());
	}
}
